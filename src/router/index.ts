import { createRouter, createWebHistory } from "@ionic/vue-router";
import { RouteRecordRaw } from "vue-router";
import Tabs from "../views/Tabs.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/tabs/tab7",
  },
  {
    path: "/tabs/",
    component: Tabs,
    children: [
      {
        path: "",
        redirect: "/tabs/tab7",
      },
      {
        path: "tab1",
        component: () => import("@/views/Tab1.vue"),
      },
      {
        path: "tab2",
        component: () => import("@/views/Tab2.vue"),
      },
      {
        path: "tab3",
        component: () => import("@/views/Tab3.vue"),
      },
      {
        path: "tab4",
        component: () => import("@/views/Tab4.vue"),
      },
      {
        path: "tab5",
        component: () => import("@/views/Tab5.vue"),
      },
      {
        path: "tab6",
        component: () => import("@/views/Tab6.vue"),
      },
      {
        path: "tab7",
        component: () => import("@/views/Tab7.vue"),
      },
      {
        path: "tab8",
        component: () => import("@/views/Tab8.vue"),
      },
      {
        path: "tab9",
        component: () => import("@/views/Tab9.vue"),
      },
      {
        path: "tab10",
        component: () => import("@/views/Tab10.vue"),
      },
      {
        path: "tab11",
        component: () => import("@/views/Tab11.vue"),
      },
      {
        path: "tab12",
        component: () => import("@/views/Tab12.vue"),
      },
      {
        path: "SignIn1",
        component: () => import("@/views/SignIn1.vue"),
      },
      {
        path: "ForgetPass",
        component: () => import("@/views/ForgetPass.vue"),
      },
      {
        path: "Register",
        component: () => import("@/views/Register.vue"),
      },
      {
        path: "Emergency",
        component: () => import("@/views/Emergency.vue"),
      },
      {
        path: "DocterDay",
        component: () => import("@/views/DocterDay.vue"),
      },
      {
        path: "postpone",
        component: () => import("@/views/postpone.vue"),
      },
      {
        path: "Payment1",
        component: () => import("@/views/Payment1.vue"),
      },
      {
        path: "Payment2",
        component: () => import("@/views/Payment2.vue"),
      },
      {
        path: "Payment3",
        component: () => import("@/views/Payment3.vue"),
      },
      {
        path: "Payment4",
        component: () => import("@/views/Payment4.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
